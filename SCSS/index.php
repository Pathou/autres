<?php
require "scss.inc.php";
$scss = new scssc();
$scss->setImportPaths("test/");

// scss_formatter -> v.dev; scss_formatter_compressed -> v.production
$scss->setFormatter('scss_formatter');

// will search for `test/style.scss'
//echo $scss->compile('@import "style.scss"');
file_put_contents('test/style.css', $scss->compile('@import "style.scss"'));

/*
 * // Voir le css généré => http://localhost/SVN_hub/SCSS/index.php/style.scss
$directory = "test";
scss_server::serveFrom($directory);*/
?>
<!DOCTYPE html>
<html>
	<head>
        <title>SCSS Génération</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css" />
        <script type="text/javascript">
        </script>
        <style type="text/css">
			
        </style>
	</head>
	<body>
		<div class="red">.red</div>
		<div class="blue">.blue</div>
		<div class="blue-lighter">.blue-lighter</div>

		<div id="debug"></div>

	</body>
</html>