<?php
/**
 * Récupère un array ini à partir d'un fichier .ini
 * @param type $fichier
 * @return array ini
 */
function get_ini($fichier) {
	$array = array();
	if (file_exists($fichier) && $fichier_lecture = file($fichier)) {
		foreach ($fichier_lecture as $ligne) {
			if (preg_match("#^\[(.*)\]\s+$#", $ligne, $matches)) {
				$groupe = trim($matches[1]);
				$array[$groupe] = array();
			} elseif ($ligne[0] != ';') {
				list($item, $valeur) = explode('=', $ligne, 2);
				if (!isset($valeur)) // S'il n'y a pas de valeur
					$valeur = ''; // On donne une chaîne vide
				$array[$groupe][trim($item)] = trim($valeur);
			}
		}
		return $array;
	}
	else
		return false;//echo 'Le fichier est introuvable ou incompatible';
}

/**
 * Sauvegarde un array ini dans un fichier
 * @param type $fichier
 * @param type $array
 */
function save_ini($fichier, $array) {
	if (is_array($array)) {
		$save = '';
		foreach ($array as $key => $groupe) {
			$save .= PHP_EOL . '[' . $key . ']';
			foreach ($groupe as $key => $item) {
				$save .= PHP_EOL . $key . '=' . $item;
			}
		}
		$save = substr($save, 2); // On enlève le premier caractère qui est une entrée à la ligne inutile 
		if (@file_put_contents($fichier, $save) === false) {
			echo 'Impossible d\'écrire dans ce fichier';
		}
	}
	else
		echo 'Fichier d\'écriture corrompu';
}

/**
 * Transforme un array ini en fichier
 * @param type $array
 * @return texte
 */
function ini2txt($array) {
	$txt = null;
	foreach ($array as $groupe => $items) {
		$txt .= '[' . $groupe . ']' . PHP_EOL;
		foreach ($items as $key => $value) {
			$txt .= $key . '=' . $value . PHP_EOL;
		}
	}
	return $txt;
}

/**
 * Transforme un texte en array ini
 * @param type $txt
 * @return array ini
 */
function txt2ini($txt) {
	$array = array();
	$ini = explode(PHP_EOL, $txt);
	foreach ($ini as $ligne) {
		if (preg_match("#^\[(.*)\]\s*$#", $ligne, $matches)) {
			$groupe = trim($matches[1]);
			$array[$groupe] = array();
		} elseif ($ligne[0] != ';') {
			list($item, $valeur) = explode('=', $ligne, 2);
			if (!isset($valeur))
				$valeur = '';
			$array[$groupe][trim($item)] = trim($valeur);
		}
	}
	return $array;
}

if (isset($_POST['submit']))
	save_ini('test.ini', $_POST['ini']);
elseif (isset($_POST['submit_txt']))
	file_put_contents('test.ini', $_POST['textarea_file']);
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test ini</title>
    </head>
    <body>
		<form action="" method="post" style="width: 600px; margin: auto; border-left: 1px solid #000000; border-right: 1px solid #000000; padding: 0 20px;">
			<h1> Test ini </h1>
<?php
foreach (get_ini('test.ini') as $groupe => $items) { ?>
			<h3><?php echo $groupe; ?></h3><?php
	foreach ($items as $key => $value) {
		?>
				<label for="<?php echo $key; ?>"><?php echo $key; ?></label> <input type="text" name="<?php echo 'ini[' . $groupe . '][' . $key . ']'; ?>" id="<?php echo 'ini[' . $groupe . '][' . $key . ']'; ?>" value="<?php echo $value; ?>" /><br />
	<?php
	}
}
?>
			<hr />
			<input type="submit" name="submit" value="Sauvegarder" />
		</form>
		<form action="" method="post" style="width: 600px; margin: auto; border-left: 1px solid #000000; border-right: 1px solid #000000; padding: 0 20px;">
			<h2 id="file" style="cursor: pointer;">Fichier</h2>
			<textarea style="width: 100%; display: none;" rows="20" id="textarea_file" name="textarea_file"><?php echo file_get_contents('test.ini'); ?></textarea>
			<hr />
			<input type="submit" name="submit_txt" value="Sauvegarder" />
		</form>

		<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
		<script type="text/javascript">
			$(function() {
				$('#file').click( function() {
					$('#textarea_file').stop(true,true).slideToggle();
				});
			});
		</script>
    </body>
</html>
