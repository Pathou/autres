<!DOCTYPE html>
<html>
<head>
        <title>Server-Sent Events - One Way Messaging</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
        <script type="text/javascript">

                (function($) {
                        $.fn.sse = function(url)
                        {
                                var result = $(this);
                                
                                if(typeof(EventSource)!=="undefined")
                                {
                                        var source=new EventSource(url);
                                        console.log(source);
                                        source.onmessage=function(event)
                                        {
                                                result.html(event.data);
                                        };
                                        /*source.addEventListener('message', function(e) {
                                                console.log(e.data);
                                        }, false);*/
                                }
                                else // IE ...
                                {
                                        setInterval(function() {
                                                $.post(url, function(data) {
                                                        result.html(data.substr(5));
                                                }); } , 3000);
                                }
                        };
                })(jQuery);
                
                
                $(function() {
                        $('#result').sse('sse.php');
                });
        </script>
        <style type="text/css">
        </style>
</head>
<body>

        <div id="result"></div>

<div id="debug"></div>

</body>
</html>