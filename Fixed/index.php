<!DOCTYPE html>
<html>
	<head>
        <title>Plug-in JQuery - Fixed</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
        <script type="text/javascript">

			(function($) {
				$.fn.fixed = function(options)
				{
					var defauts =
					{
						'offset': 5
					};  

					var parametres = $.extend(defauts, options);

					return this.each(function()
					{
						var $this = $(this);
						var $window = $(window);
						var pos_top = $this.offset().top - 5;
						var pos_left = $this.offset().left - 0;
						
						$window.on('scroll', function(){
							if($window.scrollTop() >= pos_top-parametres.offset) {
								if($this.css('position') != 'absolute' && !$this.hasClass('fixed-absolute')) $this.addClass('fixed').css({'left': pos_left+'px', 'top': parametres.offset+'px'});
								else $this.addClass('fixed fixed-absolute').css({'left': pos_left+'px'});
							}
							else {
								$this.removeClass('fixed fixed-absolute');
							}
						});
					});
				};
			})(jQuery);
                
                
			$(function() {
				$('.demo').fixed();
				$('#option').fixed({'offset': 75});
				
				
				/*$('#btn-normal').click( function() {
					$('.hide').hide();
					$('#normal').show();
				});
				$('#btn-option').click( function() {
					$('.hide').hide();
					$('#option').show();
				});
				$('#btn-margin').click( function() {
					$('.hide').hide();
					$('#margin').show();
				});*/
			});
        </script>
        <style type="text/css">
			.fixed {
				position: fixed !important;
				/*top: 5px !important;*/
				margin: 0px !important;
			}
			.fixed-absolute {
				top: 5px !important;
			}
			
			.demo, #option {
				display: inline-block;
				box-shadow: 0px 0px 5px 0px #656565;
				padding: 6px;

				background: #ffffff;
				background: -moz-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(47%,#f6f6f6), color-stop(100%,#ededed));
				background: -webkit-linear-gradient(top, #ffffff 0%,#f6f6f6 47%,#ededed 100%);
				background: -o-linear-gradient(top, #ffffff 0%,#f6f6f6 47%,#ededed 100%);
				background: -ms-linear-gradient(top, #ffffff 0%,#f6f6f6 47%,#ededed 100%);
				background: linear-gradient(to bottom, #ffffff 0%,#f6f6f6 47%,#ededed 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 );
			}
			
			#option {
				float: left;
				margin-top: 125px;
				margin-left: 10px;
			}
			
			#absolute {
				 position: absolute;
				 top: 350px; left: 650px;
			}
			#margin {
				float: left;
				margin-top: 200px;
				margin-left: 200px;
			}
			#relative {
				position: relative;
				top: 150px; left: 800px;
			}
			#float {
				float: right;
				margin-top: 600px;
				margin-right: 75px;
			}
        </style>
	</head>
	<body>
		<div style="height: 2500px; background-color: #DDDDDD; border: 1px solid #888888; width: 80px; float: right;">&nbsp;</div>

		<div class="demo hide" id="margin">Demo Test<br /><strong>Base margin</strong></div>
        <div class="demo hide" id="normal">Demo Test<br /><strong>Base normale</strong></div>
        <div class="hide "id="option">Demo Test<br /><strong>Base Option</strong><br /><strong style="font-size: 0.9em; color: #BBBBBB;">{'offset': 75}</strong></div>
		<div class="demo" id="absolute">Demo Test<br /><strong>Base absolute</strong></div>
		<div class="demo" id="float">Demo Test<br /><strong>Base float</strong></div>
		
		<div style="clear: both">&nbsp;</div>

		<div id="debug"></div>

	</body>
</html>